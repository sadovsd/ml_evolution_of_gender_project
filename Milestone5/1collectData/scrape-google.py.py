import time
import base64
from io import BytesIO
import re
import os

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
import requests
from PIL import Image
'''
Note: This repository is a commented clone of:
https://github.com/ohyicong/Google-Image-Scraper/
The reason the following code works and other google image scrapers do not work
is that google has a small protection, where users must first click on an
image in order to access data. 
The main time complexity from this scraper arises from creating the queries
and waiting for the scraper to finish. Additionally, errors occurred
when running in quick succession occasionally. In general, the scraper
worked very well!

IDE of choice: spyder
It is not recommended to edit this project in jupyter notebooks.
Other repositories for web scrapers attempted: 
    https://github.com/impshum/Multithreaded-Reddit-Image-Downloader
    note: reddit image downloader works, but it produces very biased images.
    Other google scraping attempts:
    https://pypi.org/project/google_images_download/
    https://github.com/ohyicong/Google-Image-Scraper/
    https://serpapi.com/blog/scrape-google-images-with-python/
       Note: projects did not work, need to perform workaround.


Note: The sleep methods are really important when scraping. When removing
some of them, errors occur either due to the page not loading fast enough
or a detection happening.
    
'''
cwd = os.getcwd()
IMAGE_FOLDER = 'download'
os.makedirs(
    name=f'{cwd}/{IMAGE_FOLDER}',
    exist_ok=True
)
# ensure chrome driver is installed
service = Service(ChromeDriverManager().install())
driver = webdriver.Chrome(
    service=service
)
# sleep to register as a user
SLEEP_TIME = 1

def download_google_images(search_query: str, number_of_images: int) -> str:
    '''
        Download google images with this function\n
       Takes -> search_query, number_of_images\n
       Returns -> None
    '''

    def scroll_to_bottom():
        '''Scroll to the bottom of the page, in case we are downloading
        a large number of images.
        '''
        last_height = driver.execute_script('return document.body.scrollHeight')
        while True:
            driver.execute_script('window.scrollTo(0, document.body.scrollHeight)')
            time.sleep(SLEEP_TIME)

            new_height = driver.execute_script('return document.body.scrollHeight')
            try:
                element = driver.find_element(
                    by=By.CSS_SELECTOR,
                    value='.YstHxe input'
                )
                element.click()
                time.sleep(SLEEP_TIME)
            except:
                pass

            if new_height == last_height:
                break

            last_height = new_height

    url = 'https://images.google.com/'

    driver.get(
        url=url
    )
    '''
       find search input field
    '''
    box = driver.find_element(
        by=By.XPATH,
        value="//input[contains(@class,'gLFyf gsfi')]"
    )

    box.send_keys(search_query)
    box.send_keys(Keys.ENTER)
    time.sleep(SLEEP_TIME)
    '''
       call our scroll method.
    '''
    scroll_to_bottom()
    time.sleep(SLEEP_TIME)
    '''
       find all images that contain these classes.
    '''
    img_results = driver.find_elements(
        by=By.XPATH,
        value="//img[contains(@class,'rg_i Q4LuWd')]"
    )

    total_images = len(img_results)

    print(f'Total images - {total_images}')
    '''
       purpose of variable count:
           count number of images actually able to be downloaded.
           Some images are unable to be downloaded for various reasons
    '''
    count = 0
    for img_result in img_results:
        try:
            '''
               Wait until element is clickable to avoid google from
               disconnecting us
            '''
            WebDriverWait(
                driver,
                15
            ).until(
                EC.element_to_be_clickable(
                    img_result
                )
            )
            img_result.click()
            time.sleep(SLEEP_TIME)
            '''
            actual_imgs represent images that were clicked on with
            a class that contains a larger image
            '''
            actual_imgs = driver.find_elements(
                by=By.XPATH,
                value="//img[contains(@class,'n3VNCb')]"
            )

            src = ''
            '''
             loop through actual_images, which are typically just one
             because it is the image clicked on.
            '''
            for actual_img in actual_imgs:
                if 'https://encrypted' in actual_img.get_attribute('src'):
                    pass
                elif 'http' in actual_img.get_attribute('src'):
                    src += actual_img.get_attribute('src')
                    break
                else:
                    pass

            for actual_img in actual_imgs:
                if src == '' and 'base' in actual_img.get_attribute('src'):
                    src += actual_img.get_attribute('src')

            if 'https://' in src:
                image_name = search_query.replace('/', ' ')
                image_name = re.sub(pattern=" ", repl="_", string=image_name)
                file_path = f'{IMAGE_FOLDER}/{count}_{image_name}.jpeg'
                try:
                    '''
                       Send a request to get this image data from the url
                    '''
                    result = requests.get(src, allow_redirects=True, timeout=10)
                    open(file_path, 'wb').write(result.content)
                    img = Image.open(file_path)
                    img = img.convert('RGB')
                    '''
                    save image to os using file path, which was modified
                    from the image number and the search query.
                    '''
                    img.save(file_path, 'JPEG')
                    print(f'Count - {count} - Image saved from https.')
                except:
                    print('Bad image.')
                    try:
                        os.unlink(file_path)
                    except:
                        pass
                    count -= 1
            else:
                '''
                   We do not have http in source, we can safely open image
                   normally
                '''
                img_data = src.split(',')
                image_name = search_query.replace('/', ' ')
                image_name = re.sub(pattern=" ", repl="_", string=image_name)
                file_path = f'{IMAGE_FOLDER}/{count}_{image_name}.jpeg'
                try:
                    img = Image.open(BytesIO(base64.b64decode(img_data[1])))
                    img = img.convert('RGB')
                    img.save(file_path, 'JPEG')
                    print(f'Count - {count} - Image saved from Base64.')
                except:
                    print('Bad image.')
                    count -= 1
        except ElementClickInterceptedException as e:
            # Page failed to load: This should NEVER happen.
            count -= 1
            print(e)
            print('Image is not clickable.')
            driver.quit()

        count += 1
        # we successfuly met the count! Exit out of the loop
        if count >= total_images:
            print('No more images to download.')
            break
        if count == number_of_images:
            break
# where to search
tags = [
    "1920s family"
]
# Search queries attempted:
# "old portraits", "Cabinet Cards"]))
# "early 1900s photography"
# "1800s pictures"
# "early 1900s photography"
# "Candid old photos", photos from the 1980s, photos from the 1930s, from 1940s, 90s
# 60s, 50s, 1890s
# ambro deuernan, tintypes
# 1900s photos of men
# 1900s photos of men with mustaches
# 1930s men, women
# 20th century men/women
# mid 20th century family
# 1960s family
# 40s family
for tag in tags:
    print(f'{"="*10} Downloding for the tag - {tag} {"="*10}')
    download_google_images(
        tag,
        # number of images to download from each search tag
        50
    )
    print(f'{"="*10} Finished downloding for the tag - {tag} {"="*10}')

driver.quit()