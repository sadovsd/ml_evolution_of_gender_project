# This script gets the number of men, women, and images in a directory. 
# We use it to calculate the number of men, women, and images in the training and validation set for each fold. 
import os

def get_num_men_women(directory, list_num_women, list_num_men, list_num_images):
    '''Get the number of men, women, and images in the directory, and add the results to the respective lists'''
    num_women = 0
    num_men = 0
    # Avoid hidden . files
    files = [file for file in os.listdir(directory) if file[0] != '.']
    num_images = len(files)
    for filename in files:
        # Ignore sub directories
        if filename == 'train_images' or filename == 'test_images':
            continue
        with open(f'{directory}/{filename}', 'r', encoding='latin-1') as input:
            for line in input:
                # Women coded as 0
                if line[0] == '0':
                    num_women += 1
                # Men coded as 1
                elif line[0] == '1':
                    num_men += 1
    list_num_women.append(num_women)
    list_num_men.append(num_men)
    list_num_images.append(num_images)

train_num_women, train_num_men, train_num_images = [], [], []
val_num_women, val_num_men, val_num_images = [], [], []
# Get the number of men, women, and images in the training and validation set for each of the 10 folds
for i in range(1, 11):
    get_num_men_women(f'/home/schuerr2/kfold_datasets/fold_{i}/labels/train2017/', train_num_women, train_num_men, train_num_images)
    get_num_men_women(f'/home/schuerr2/kfold_datasets/fold_{i}/labels/val2017/', val_num_women, val_num_men, val_num_images)

with open('class_count_train_test.txt', 'w') as output:
    print(f'Number of women in training set: {train_num_women}', file=output)
    print(f'Number of men in training set: {train_num_men}', file=output)
    print(f'Number of images in training set: {train_num_images}', file=output)
    print(f'Number of women in validation set: {val_num_women}', file=output)
    print(f'Number of men in validation set: {val_num_men}', file=output)
    print(f'Number of images in validation set: {val_num_images}', file=output)
