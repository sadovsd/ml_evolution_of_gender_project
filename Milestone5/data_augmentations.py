# This script augments the training data in each fold
import albumentations as A
import random
import numpy as np
from PIL import Image
import os
import shutil

# Create individual augmentations
equalize = A.Compose([A.Equalize(p=1)], bbox_params=A.BboxParams(format='yolo', min_visibility=0.3))
hflip = A.Compose([A.HorizontalFlip(p=1)], bbox_params=A.BboxParams(format='yolo', min_visibility=0.3))
# Shear, reasonable degree values are between -40 to 40
shear = A.Compose([A.Affine(shear=random.randrange(-40, 40, 1))], bbox_params=A.BboxParams(format='yolo', min_visibility=0.3))
crop = A.Compose([A.RandomSizedBBoxSafeCrop(p=1, height=500, width=500)], bbox_params=A.BboxParams(format='yolo', min_visibility=0.3))

# Create pipeline of augmentations
hflip_equalize = A.Compose([
    A.HorizontalFlip(p=1),
    A.Equalize(p=1)
], bbox_params=A.BboxParams(format='yolo', min_visibility=0.3))
shear_hflip_equalize = A.Compose([
    A.Affine(shear=random.randrange(-40, 40, 1)),
    A.HorizontalFlip(p=0.5),
    A.Equalize(p=1)
], bbox_params=A.BboxParams(format='yolo', min_visibility=0.3))
crop_hflip_equalize = A.Compose([
    A.RandomSizedBBoxSafeCrop(p=1, height=500, width=500),
    A.HorizontalFlip(p=0.5),
    A.Equalize(p=1)
], bbox_params=A.BboxParams(format='yolo', min_visibility=0.3))


augmentations = [equalize, hflip, shear, crop, hflip_equalize, shear_hflip_equalize, crop_hflip_equalize]
augmentation_names = ['equalize', 'hflip', 'shear', 'crop', 'hflip_equalize', 'shear_hflip_equalize', 'crop_hflip_equalize']


def format_bboxes(bboxes):
    '''Helper function to format bounding boxes in YOLO format'''
    updated_bboxes = ''
    for box in bboxes:
        # Round to 6 decimals and make each number have 6 decimals like the original data other than the class
        box = [format(round(box[-1], 6), '.6f')] + [format(round(x, 6), '.6f') for x in box[:-1]]
        updated_bboxes += f'{box[0]} {box[1]} {box[2]} {box[3]} {box[4]}\n'
    return updated_bboxes

fold = 1

# Augment each fold since each training/validation set needs its own directory for YOLOv5
for fold in range(1, 11):
    print(f'On fold {fold}')

    # Copy directory
    shutil.copytree(f'/home/schuerr2/kfold_datasets/fold_{fold}', f'/home/schuerr2/kfold_datasets/augmented_fold_{fold}', copy_function = shutil.copy) 

    # Get list of training images
    files = os.listdir(f'/home/schuerr2/kfold_datasets/augmented_fold_{fold}/images/train2017/')
    count = 1

    # Augment each training image
    for file in files:
        try:
            print(f'On file {count} of {len(files)}')
            count += 1

            # Open image and labels
            image = Image.open(f'/home/schuerr2/kfold_datasets/augmented_fold_{fold}/images/train2017/{file}')
            image = np.array(image)

            with open(f'/home/schuerr2/kfold_datasets/augmented_fold_{fold}/labels/train2017/{file[:-4]}.txt') as input:
                file_bboxes = []
                for line in input:
                    split_line = line.strip('\n').split(' ')
                    # Convert to floats and round to nearest 6 decimals. Got an error later with negative floating point for 0
                    split_line = [round(float(x), 6) for x in split_line]
                    # convert class to int
                    split_line[0] = int(split_line[0])
                    file_bboxes.append(split_line[1:] + [split_line[0]])

            # Perform each augmentation
            for i in range(len(augmentations)):
                try:
                    transformed = augmentations[i](image=image, bboxes=file_bboxes)
                    transformed_image = transformed['image']
                    transformed_bboxes = transformed['bboxes']
                    im = Image.fromarray(transformed_image)
                    im.save(f'/home/schuerr2/kfold_datasets/augmented_fold_{fold}/images/train2017/{file[:-4]}_{augmentation_names[i]}{file[-4:]}')
                    formatted_boxes = format_bboxes(transformed_bboxes)
                    with open(f'/home/schuerr2/kfold_datasets/augmented_fold_{fold}/labels/train2017/{file[:-4]}_{augmentation_names[i]}.txt', 'w') as output:
                        output.write(formatted_boxes)
                except Exception as e:
                    print(e)
        except Exception as e:
            print(e)


