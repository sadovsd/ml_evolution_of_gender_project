# This script creates 10 folds for cross validation. It balances the number of men and women in each fold. 

from iterstrat.ml_stratifiers import MultilabelStratifiedKFold
import numpy as np
import pandas as pd
import os
import shutil

# Read in csv containing the number of men and women in each image
df = pd.read_csv('obj_Train_data_males_females.csv')

x = df.loc[:, 'file'].to_numpy()
y = df.loc[:, ['num_males', 'num_females']].to_numpy()

# Create 10 folds
mskf = MultilabelStratifiedKFold(n_splits=10, shuffle=True, random_state=0)

fold = 1

# Iterate through all the folds
for train_index, test_index in mskf.split(x, y):
    print(f'On fold {fold}')

    # Make directories
    base_dir = f'/home/schuerr2/kfold_datasets/fold_{fold}'
    os.mkdir(base_dir)
    os.mkdir(f'{base_dir}/images')
    os.mkdir(f'{base_dir}/images/train2017')
    os.mkdir(f'{base_dir}/images/val2017')
    os.mkdir(f'{base_dir}/labels')
    os.mkdir(f'{base_dir}/labels/train2017')
    os.mkdir(f'{base_dir}/labels/val2017')
    
    # Copy data to new directory for the fold. Note, we copy the data since each YOLOv5 training/validation set requires the data to be in its own directory
    for index in train_index:
        filename = df.loc[index, "file"]
        shutil.copyfile(f'/home/schuerr2/obj_Train_data/train_images/{filename + ".jpg"}', f'{base_dir}/images/train2017/{filename + ".jpg"}')
        shutil.copyfile(f'/home/schuerr2/obj_Train_data/{filename + ".txt"}', f'{base_dir}/labels/train2017/{filename + ".txt"}')
        
    for index in test_index:
        filename = df.loc[index, "file"]
        shutil.copyfile(f'/home/schuerr2/obj_Train_data/train_images/{filename + ".jpg"}', f'{base_dir}/images/val2017/{filename + ".jpg"}')
        shutil.copyfile(f'/home/schuerr2/obj_Train_data/{filename + ".txt"}', f'{base_dir}/labels/val2017/{filename + ".txt"}')

    fold += 1