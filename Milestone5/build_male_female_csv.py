# This script builds a csv where each row is a file in the directory and the number of males and females it has in it.
# This csv files used to make stratified k-folds in create_k_folds.py 
import os

def build_csv(directory):
    '''Build csv where each row is a file in the directory and the number of males and females it has in it'''
    # Avoid hidden . files
    files = [file for file in os.listdir(directory) if file[0] != '.']
    with open (f'{directory}_males_females.csv', 'w') as output:
        output.write('file,num_males,num_females\n')

        for filename in files:
            num_males = 0
            num_females = 0
            if filename == 'train_images' or filename == 'test_images':
                continue
            with open(f'{directory}/{filename}', 'r', encoding='latin-1') as input:
                for line in input:
                    # Females coded as 0
                    if line[0] == '0':
                        num_females += 1
                    # Males coded as 1
                    elif line[0] == '1':
                        num_males += 1

            output.write(f'{filename[:-4]},{num_males},{num_females}\n')

build_csv('/home/schuerr2/obj_Train_data')