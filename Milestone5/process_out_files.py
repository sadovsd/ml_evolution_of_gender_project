import os

files_to_delete = []

trained_models = []
successful_out_files = []

training_models = []

# Process all the .out files in the directory
for file in os.listdir('.'):
    if file[-4:] == '.out':
        # Track whether model is still training
        training = True
        with open(file, 'r') as input:
            for line in input:
                split_line = line.strip('\n').split(' ')

                if 'train:' in split_line[0]:
                    index_of_name = line.find('name=')
                    if index_of_name != -1:
                        model_name = line[index_of_name + 5: line.find(',', index_of_name)]

                # If a runtime error occurred, store the name of the file, so we can delete it later
                if split_line[0] == 'RuntimeError:':
                    files_to_delete.append(file)
                    training = False

                # Store the model name and out file of a successfully trained model
                if split_line[0] == 'Results' and split_line[1] == 'saved':
                    # Extract and store the model name
                    model_name = split_line[3][split_line[3].rfind('/') + 1:-4]
                    trained_models.append(model_name)

                    # Store the out file name
                    successful_out_files.append(file)

                    training = False
        # Add the model to the list of training models if it hasn't finished training
        if training == True:
            training_models.append(model_name)

print(f'Running models: {training_models}')

print(f'Trained models: {trained_models}')

print(f'Successful out files: {successful_out_files}')

# Delete unsuccessful training runs
for file in files_to_delete:
    os.remove(f'/home/schuerr2/train_jobs/{file}')

# Move successfully trained models' out files to new directory and rename them after model
for i in range(len(successful_out_files)):
    os.rename(f'/home/schuerr2/train_jobs/{successful_out_files[i]}', f'/home/schuerr2/train_jobs/successful_training_runs/{trained_models[i]}.out')