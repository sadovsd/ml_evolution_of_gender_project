In 1collectData, we have our web scraper that was used to gather images from google.

To replicate our results, all of the following must be done on Miami University's Redhawk High-Performance Computing Cluster, or the results from steps 1 to 4 must all be uploaded to it before performing steps 5 and 6. 

To replicate our results, follow the following process:
1. Download train:test_dataset_with_images.zip (available at https://drive.google.com/file/d/1LHXnQ5CKTjnu0wXDtLthHScy6fqbPBLR/view?usp=share_link)
2. Run build_male_female_csv.py (available at https://drive.google.com/file/d/11K6KybSXWxeSg7K9IhJ6O5R0wqmsMhEw/view?usp=sharing) after you change the directory on line 28 to the proper path containing the training data downloaded in step 1.
3. Run create_k_folds.py (available at https://drive.google.com/file/d/1SSDM49JJ9sV8NmE3Psr_Rvnnd7pyajYM/view?usp=share_link) once you have replaced the directories on lines 25, 37, 38, 42, and 43 with the correct local paths you have set up. 
4. Run data_augmentations.py (available at https://drive.google.com/file/d/12dmmfUEtiBljN7B_xS33Slv0RlOZQUnT/view?usp=share_link) once you have changed the directories on lines 53, 56, 66, 69, 86, and 88 to the local ones you have set up
5. Clone YOLOv5 from https://github.com/ultralytics/yolov5
6. Run train.py (available at https://drive.google.com/file/d/11ooFdy-aDrWHnTZMtZFmcpVutKkXvD4Y/view?usp=share_link) on Miami University's Redhawk cluster to train all 60 models: augmented and unaugmented with none of the model frozen, the backbone frozen, and all but the last layer frozen on each of the 10 folds. 

We are still waiting for some of the models to train, so we do not have results yet. 
