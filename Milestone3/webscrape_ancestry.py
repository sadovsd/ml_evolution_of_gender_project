from bs4 import BeautifulSoup
import requests as r
from time import sleep
from random import randint

web_pages = ["https://www.ancestry.com/search/collections/1093/?event=1900&count=50&event_x=10-0-0&keyword=peopleeople",
             "https://www.ancestry.com/search/collections/1093/?event=1900&count=50&event_x=10-0-0&keyword=peopleeople&fh=100&fsk=MDs5OTs1MA-61--61-",
             "https://www.ancestry.com/search/collections/1093/?event=1900&count=50&event_x=10-0-0&keyword=peopleeople&fh=250&fsk=MDsyNDk7NTA-61-",
             "https://www.ancestry.com/search/collections/1093/?event=1900&count=50&event_x=10-0-0&keyword=peopleeople&fh=400&fsk=MDszOTk7NTA-61-",
             "https://www.ancestry.com/search/collections/1093/?event=1900&count=50&event_x=10-0-0&keyword=peopleeople&fh=550&fsk=MDs1NDk7NTA-61-",
             "https://www.ancestry.com/search/collections/1093/?event=1900&count=50&event_x=10-0-0&keyword=peopleeople&fh=700&fsk=MDs2OTk7NTA-61-",
             "https://www.ancestry.com/search/collections/1093/?event=1900&count=50&event_x=10-0-0&keyword=peopleeople&fh=700&fsk=MDs2OTk7NTA-61-",
             "https://www.ancestry.com/search/collections/1093/?event=1900&count=50&event_x=10-0-0&keyword=peopleeople&fh=750&fsk=MDs3NDk7NTA-61-",
             "https://www.ancestry.com/search/collections/1093/?event=1900&count=50&event_x=10-0-0&keyword=peopleeople&fh=850&fsk=MDs4NDk7NTA-61-",
             "https://www.ancestry.com/search/collections/1093/?event=1900&count=50&event_x=10-0-0&keyword=peopleeople&fh=1100&fsk=MDsxMDk5OzUw",
             "https://www.ancestry.com/search/collections/1093/?event=1900&count=50&event_x=10-0-0&keyword=peopleeople&fh=1350&fsk=MDsxMzQ5OzUw",
             "https://www.ancestry.com/search/collections/1093/?event=1900&count=50&event_x=10-0-0&keyword=peopleeople&fh=1400&fsk=MDsxMzk5OzUw",
             "https://www.ancestry.com/search/collections/1093/?event=1900&count=50&event_x=10-0-0&keyword=peopleeople&fh=1600&fsk=MDsxNTk5OzUw",
            ]

i = 0
for url in web_pages:
    agent = {"User-Agent":"Mozilla/5.0"}
    page_source = r.get(url, headers=agent).text
    soup = BeautifulSoup(page_source, 'lxml')
    # Extract the images from a url
    imgs = soup.find_all('img')
    for item in imgs:
        i += 1
        link = item['src']
        with open(f"images_final/img{i}.jpg", 'wb') as f:
            im = r.get(link)
            f.write(im.content)
    # Make sure ip doesn't get banned
    sleep(randint(2,6))