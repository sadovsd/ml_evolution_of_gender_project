# This script will get the number of men and women in a directory. We use it to calculate the number of men and women in the training and testing set.
import os

def get_num_men_women(directory):
    num_women = 0
    num_men = 0
    for filename in os.listdir(directory):
        with open(f'{directory}/{filename}', 'r', encoding='latin-1') as input:
            for line in input:
                # Women coded as 0
                if line[0] == '0':
                    num_women += 1
                # Men coded as 1
                elif line[0] == '1':
                    num_men += 1
    return num_women, num_men

train_num_women, train_num_men = get_num_men_women('postcard_dataset_yolo/labels/train2017/')
test_num_women, test_num_men = get_num_men_women('postcard_dataset_yolo/labels/val2017/')

with open('class_count_train_test.txt', 'w') as output:
    print(f'Number of women in training set: {train_num_women}', file=output)
    print(f'Number of men in training set: {train_num_men}', file=output)
    print(f'Number of women in testing set: {test_num_women}', file=output)
    print(f'Number of men in testing set: {test_num_men}', file=output)

