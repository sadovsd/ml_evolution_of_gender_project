# This file will process predictions made by the YOLOv5 models, both trained and pre-trained, and update
# the dataset with the number of men, women, and people, whether they were present in an image, and other
# objects detected by the pre-trained YOLOv5x model

import pandas as pd
import os

from helper_functions import read_bboxes, eliminate_overlapping_bounding_boxes

# Create Mapping for Yolov5 Pretrained Classes
pretrained_predictions = {
  0: 'person',
  1: 'bicycle',
  2: 'car',
  3: 'motorcycle',
  4: 'airplane',
  5: 'bus',
  6: 'train',
  7: 'truck',
  8: 'boat',
  9: 'traffic light',
  10: 'fire hydrant',
  11: 'stop sign',
  12: 'parking meter',
  13: 'bench',
  14: 'bird',
  15: 'cat',
  16: 'dog',
  17: 'horse',
  18: 'sheep',
  19: 'cow',
  20: 'elephant',
  21: 'bear',
  22: 'zebra',
  23: 'giraffe',
  24: 'backpack',
  25: 'umbrella',
  26: 'handbag',
  27: 'tie',
  28: 'suitcase',
  29: 'frisbee',
  30: 'skis',
  31: 'snowboard',
  32: 'sports ball',
  33: 'kite',
  34: 'baseball bat',
  35: 'baseball glove',
  36: 'skateboard',
  37: 'surfboard',
  38: 'tennis racket',
  39: 'bottle',
  40: 'wine glass',
  41: 'cup',
  42: 'fork',
  43: 'knife',
  44: 'spoon',
  45: 'bowl',
  46: 'banana',
  47: 'apple',
  48: 'sandwich',
  49: 'orange',
  50: 'broccoli',
  51: 'carrot',
  52: 'hot dog',
  53: 'pizza',
  54: 'donut',
  55: 'cake',
  56: 'chair',
  57: 'couch',
  58: 'potted plant',
  59: 'bed',
  60: 'dining table',
  61: 'toilet',
  62: 'tv',
  63: 'laptop',
  64: 'mouse',
  65: 'remote',
  66: 'keyboard',
  67: 'cell phone',
  68: 'microwave',
  69: 'oven',
  70: 'toaster',
  71: 'sink',
  72: 'refrigerator',
  73: 'book',
  74: 'clock',
  75: 'vase',
  76: 'scissors',
  77: 'teddy bear',
  78: 'hair drier',
  79: 'toothbrush'}

def process_predictions(predicted_classes, class_code):
    '''Take a list of predicted_classes and return whether the class is found and the number of the class code predicted (e.g., 0 is female in our model)'''
    num_class = 0
    for predicted_class in predicted_classes:
        if predicted_class == class_code:
            num_class += 1
    class_found = True if num_class > 0 else False
    return class_found, num_class

# Calibrated on unaugmented backbone
female_threshold = 0.39
male_threshold = 0.16

# Map postcard's identifier to additional data (e.g., number of men and women in the postcard)
identifier_gender_info = {}

for directory in os.listdir('/home/schuerr2/yolov5/runs/detect'):
    # only process results for the unaugmented backbone model or the pretrained
    if ('unaug_backbone' not in directory) and ('pretrained' not in directory):
        continue
        
    directory = f'/home/schuerr2/yolov5/runs/detect/{directory}'
    
    for file in os.listdir(f'{directory}/labels'):
        # Extract the identifer from the filename
        ending_index = file.find('_Page1') if file.find('_Page1') != -1 else file.find('_Recto')
        identifier = file[:ending_index]
        
        # Read in predicted bboxes
        predicted_classes, confidences, predicted_bboxes = read_bboxes(f'{directory}/labels/{file}')
        
        if 'pretrained' in directory:
            num_people = 0
            other = {}
            for predicted_class in predicted_classes:
                if predicted_class == 0:
                    num_people += 1
                else:
                    # Increment the count of the object for the image (e.g., add 'Boat': 1 or update it to 'Boat': 2)
                    other[pretrained_predictions[predicted_class]] = other.get(pretrained_predictions[predicted_class], 0) + 1
                    
            has_people = True if num_people > 0 else False
            
            if identifier not in identifier_gender_info:
                identifier_gender_info[identifier] = {}
                
            identifier_gender_info[identifier]['people'] = has_people
            identifier_gender_info[identifier]['num_people'] = num_people
            identifier_gender_info[identifier]['other'] = other
        else:
            # Eliminate overlapping predictions using the two optimized thresholds for females and males
            refined_female_classes, _, _ = eliminate_overlapping_bounding_boxes(predicted_classes, confidences, predicted_bboxes, iou_threshold=female_threshold)
            refined_male_classes, _, _ = eliminate_overlapping_bounding_boxes(predicted_classes, confidences, predicted_bboxes, iou_threshold=male_threshold)

            females, num_females = process_predictions(refined_female_classes, 0)
            males, num_males = process_predictions(refined_male_classes, 1)
            
            if identifier not in identifier_gender_info:
                identifier_gender_info[identifier] = {}
            identifier_gender_info[identifier]['females'] = females
            identifier_gender_info[identifier]['num_females'] = num_females
            identifier_gender_info[identifier]['males'] = males
            identifier_gender_info[identifier]['num_males'] = num_males
        

# Build lists of results, which will become columns
females = []
num_females = []
males = []
num_males = []
people = []
num_people = []
other = []

# Read in the full dataset
df = pd.read_csv('full_dataset.csv')

for identifier in df['Identifier']:
    if identifier in identifier_gender_info:
        females.append(identifier_gender_info[identifier].get('females', False))
        num_females.append(identifier_gender_info[identifier].get('num_females', 0)) 
        males.append(identifier_gender_info[identifier].get('males', False))
        num_males.append(identifier_gender_info[identifier].get('num_males', 0)) 
        people.append(identifier_gender_info[identifier].get('people', False)) 
        num_people.append(identifier_gender_info[identifier].get('num_people', 0)) 
        other.append(identifier_gender_info[identifier].get('other', None)) 
    else:
        females.append(False)
        num_females.append(0)
        males.append(False)
        num_males.append(0)
        people.append(False)
        num_people.append(0)
        other.append(None)

# Add new columns
df['Females in Picture'] = females
df['Number of Females'] = num_females
df['Males in Picture'] = males
df['Number of Males'] = num_males
df['Pretrained People in Picture'] = people
df['Pretrained Number of People'] = num_people
df['Pretrained Other Objects in Picture'] = other

# Save results to csv
df.to_csv('updated_full_dataset.csv')