import os

directory = '/shared/library_image_project2022/Bowden'
for item in os.listdir(directory):
    if os.path.isdir(f'{directory}/{item}'):
        for file in os.listdir(f'{directory}/{item}'):
            if 'Page1' in file:
                os.rename(f'{directory}/{item}/{file}', f'{directory}/postcard_fronts/{file}')