from unicodedata import east_asian_width
import albumentations as A
import numpy as np
from PIL import Image
import os


# Create transformations
# Flip the image horizontally 
horizontal_flip = A.Compose([A.HorizontalFlip(p=1)],bbox_params=A.BboxParams(format='yolo', min_visibility=0.3))
# Rotate the images up to 5 degrees each way. People likely won't stand at much more than a 5 degree angle
rotate = A.Compose([A.SafeRotate(p=1, limit=[-5,5])], bbox_params=A.BboxParams(format='yolo', min_visibility=0.3))
# Randomly crop the image
crop = A.Compose([A.RandomSizedBBoxSafeCrop(p=1, width=448, height=448, erosion_rate=0.2)],bbox_params=A.BboxParams(format='yolo', min_visibility=0.3))
# Add gaussian noise
noise = A.Compose([A.GaussNoise(p=1)],bbox_params=A.BboxParams(format='yolo', min_visibility=0.3))
# Compress the image to decrease the quality of it
compress = A.Compose([A.ImageCompression(50,60,p=1)],bbox_params=A.BboxParams(format='yolo', min_visibility=0.3))
# Change brightness
brightness = A.Compose([A.RandomBrightnessContrast(p=1)],bbox_params=A.BboxParams(format='yolo', min_visibility=0.3))
augmentations = [horizontal_flip, rotate, crop, noise, compress, brightness]
augmentation_names = ['h_flip', 'rotate', 'crop', 'noise', 'compress', 'brightness']


def format_bboxes(bboxes):
    '''Helper function to format bounding boxes in YOLO format'''
    updated_bboxes = ''
    for box in bboxes:
        # Round to 6 decimals and make each number have 6 decimals like the original data other than the class
        box = [format(round(box[-1], 6), '.6f')] + [format(round(x, 6), '.6f') for x in box[:-1]]
        updated_bboxes += f'{box[0]} {box[1]} {box[2]} {box[3]} {box[4]}\n'
    return updated_bboxes

files = os.listdir('augmented_postcard_dataset_yolo/images/train2017/')
count = 1
for file in files:
    try:
        print(f'On file {count} of {len(files)}')
        count += 1
        # Open image and label
        image = Image.open(f'augmented_postcard_dataset_yolo/images/train2017/{file}')
        image = np.array(image)

        with open(f'augmented_postcard_dataset_yolo/labels/train2017/{file[:-4]}.txt') as input:
            file_bboxes = []
            for line in input:
                split_line = line.strip('\n').split(' ')
                # Convert to floats and round to nearest 6 decimals. Got an error later with negative floating point for 0
                split_line = [round(float(x), 6) for x in split_line]
                # convert class to int
                split_line[0] = int(split_line[0])
                file_bboxes.append(split_line[1:] + [split_line[0]])

        # Performed each augmentation
        for i in range(len(augmentations)):
            try:
                transformed = augmentations[i](image=image, bboxes=file_bboxes)
                transformed_image = transformed['image']
                transformed_bboxes = transformed['bboxes']
                im = Image.fromarray(transformed_image)
                im.save(f'augmented_postcard_dataset_yolo/images/train2017/{file[:-4]}_{augmentation_names[i]}{file[-4:]}')
                formatted_boxes = format_bboxes(transformed_bboxes)
                with open(f'augmented_postcard_dataset_yolo/labels/train2017/{file[:-4]}_{augmentation_names[i]}.txt', 'w') as output:
                    output.write(formatted_boxes)
            except Exception as e:
                print(e)
    except Exception as e:
        print(e)


