import os
augmentations_to_delete = ['fog', 'gray', 'rgb_shift', 'channel_shuffle']

for filename in os.listdir('augmented_postcard_dataset_yolo/images/train2017/'):
    for augmentation in augmentations_to_delete:
        if augmentation in filename:
            os.remove(f'augmented_postcard_dataset_yolo/images/train2017/{filename}')
            break