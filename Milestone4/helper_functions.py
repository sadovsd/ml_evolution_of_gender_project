# Helper functions used throughout the project to evaluate and refine results

def yolobbox2bbox(x,y,w,h):
    '''Convert the bounding box from yolo format to normal format (i.e., x1, y1, x2, y2) and return the converted bbox'''
    x1, y1 = x-w/2, y-h/2
    x2, y2 = x+w/2, y+h/2
    return x1, y1, x2, y2

def get_iou(bb1, bb2):
    """
    Return the Intersection over Union (IoU) of two bounding boxes.
    """
    assert bb1[0] < bb1[2]
    assert bb1[1] < bb1[3]
    assert bb2[0] < bb2[2]
    assert bb2[1] < bb2[3]

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1[0], bb2[0])
    y_top = max(bb1[1], bb2[1])
    x_right = min(bb1[2], bb2[2])
    y_bottom = min(bb1[3], bb2[3])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1[2] - bb1[0]) * (bb1[3] - bb1[1])
    bb2_area = (bb2[2] - bb2[0]) * (bb2[3] - bb2[1])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou

def read_bboxes(filepath, predictions=True):
    '''Read in bboxes, and return the classes and bounding boxes. If we're reading predictions, also return the confidence of the predictions. '''
    predicted_classes = []
    predicted_bboxes = []
    if predictions:
        confidences = []

    try:
        with open(filepath, 'r') as input_file:
            for line in input_file:
                split_line = line.strip('\n').split(' ')
                split_line = [float(x) for x in split_line]

                 # Class is first entry and is a number (e.g., 0 is person in pretrained YOLOv5)
                predicted_classes.append(split_line[0])
                # Convert the bounding box from yolo format to normal format (i.e., x1, y1, x2, y2)
                predicted_bboxes.append(yolobbox2bbox(split_line[1], split_line[2], split_line[3], split_line[4]))
                if predictions:
                    # Confidence is the last entry on each line
                    confidences.append(split_line[-1])
    except Exception as e:
        print(e)
    
    if predictions:
        return predicted_classes, confidences, predicted_bboxes
    else:
        return predicted_classes, predicted_bboxes

def eliminate_overlapping_bounding_boxes(predicted_classes, confidences, predicted_bboxes, iou_threshold=0.7):
    '''
    Remove overlapping bounding boxes with lower confidence and return the refined, non-overlapping predictions. 
    
    This fixes the issue of the algorithm labeling someone as a man and women because we will only store the most confident prediction.
    If two bounding boxes overlap by more than the iou_threshold, we will only retain the most confident prediction. For example, if the
    model predicts two bounding boxes one male with confidence 0.5 and one female with confidence 0.4, and the bounding boxes have an IoU
    of 0.75 (greater than the default threshold of 0.7), we will only retain the male prediction. Note, the default threshold was chosen by
    going through the unaugmented model's predictions and changing the threshold until each person only had one box around them. 
    '''
    indexes_to_remove = set()
    seen_comparisions = set()
    
    for i in range(len(predicted_bboxes)):
        for j in range(len(predicted_bboxes)):
            # Don't check if it's overlapping with itself or if we've seen an equivalent comparison (e.g., compared 1 to 2, don't need to check 2 to 1)
            if (i == j) or ((i, j) in seen_comparisions) or ((j, i) in seen_comparisions):
                continue
                
            seen_comparisions.add((i, j))
                
            # If the intersection over the union is greater than the threshold, consider the bounding boxes the same
            if get_iou(predicted_bboxes[i], predicted_bboxes[j]) > iou_threshold:
                # Remove i if we are less confident in i. Else, remove j
                index_to_remove = i if confidences[i] < confidences[j] else j
                indexes_to_remove.add(index_to_remove)

    refined_predicted_classes = []
    refined_confidences = []
    refined_predicted_bboxes = []
    
    # Build refined predictions
    for i in range(len(predicted_classes)):
        if i not in indexes_to_remove:
            refined_predicted_classes.append(predicted_classes[i])
            refined_confidences.append(confidences[i])
            refined_predicted_bboxes.append(predicted_bboxes[i])
            
    return refined_predicted_classes, refined_confidences, refined_predicted_bboxes

def calculate_accuracy_precision_recall(true_positive, false_positive, true_negative, false_negative):
    '''Calculate and return accuracy, precision, recall, and f1'''
    accuracy = (true_positive + true_negative) / (true_positive + false_positive + true_negative + false_negative)
    # What proportion of positive identifications was actually correct?
    precision = true_positive / (true_positive + false_positive)
    # What proportion of actual positives was identified correctly?
    recall = true_positive / (true_positive + false_negative)
    f1 = 2 * (recall * precision) / (recall + precision)
    return accuracy, precision, recall, f1